package com.etep.myapplication1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    // Requisitos do aplicativo e 'dicas' no arquivo Task

    Button butA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        butA =(Button)findViewById(R.id.butA);

        butA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Função que chama a Activity MainActivity2

                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), MainActivity2.class);
                startActivity(intent);

                //Também pode ser escrito como:

//                Intent intent = new Intent(getApplicationContext(), MainActivity2.class);
//                startActivity(intent);
            }
        });
    }
}
