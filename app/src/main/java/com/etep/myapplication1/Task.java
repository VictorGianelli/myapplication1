package com.etep.myapplication1;

public class Task {
    /*
    * Requerimentos
    *
    * done:  um aplicativo com duas telas.
    * done:  use a função scrollview
    * done:  com um botão que leve para outra tela
    * done:  que tenha um botão de voltar para tela inicial
    * done:  que eu possa carregar imagens para dentro do aplicativo.
    * done:  Ê que possa alterar o ícone que ficará no celular.
    *
    * Dicas:
    *
    * Para a imagem do icone:
    *   Em Project
    *   Botão direito do mouse na area da aba Project e selecione New > Image Asset
    *
    *   Selecione em Icon Type: Launcher Icon (Legacy only);
    *   Defina o nome Name do icone;
    *   Selecione em Asset Type: Image;
    *   Informe o diretório da imagem DO FORMATO PNG em Path (De preferencia 1:1 ou "quadrada").
    *   [As outras opções vc pode deixar para a livre escolha dos usuarios, mas ai esta a minha sugestão padrão]
    *   *Trim: No
    *   *Padding: 0%
    *   *Bg: (indiferente para Shape = None)
    *   *Scaling: Shrink To Fit
    *   *Shape: None
    *   *Effect: None
    *
    *   Clicar em Next (O icone que tem nesse projeto é de um app q eu tava fazendo)
    *
    *   Selecione em Res Directory: main e aperte em Finish
    *
    * Para alterar o ícone que ficará exposto:
    *   Em Project
    *   Clique em app > manifest > Android manifest
    *   Em android:icon="@mipmap/ic_launcher" escreva @mipmap/'nome dado ao icone'
    * */
}
