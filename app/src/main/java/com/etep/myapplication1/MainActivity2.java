package com.etep.myapplication1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {
    // Requisitos do aplicativo e 'dicas' no arquivo Task

    Button butB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }

    public void butB(View view) {
        //Chama a Activity MainActivity
//        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//        startActivity(intent);

//       ou

        //Encerra a Activity atual
        finish();
    }
}
